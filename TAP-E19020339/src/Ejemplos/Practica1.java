/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplos;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.*;
/**
 *
 * @author gaby-
 */
public class Practica1 extends JFrame  {
    
    JButton btnCerrar;
    
    Practica1(){
    this.setSize(300, 200);
    this.setTitle("Practica 1");
    
    btnCerrar = new JButton("Cerrar");
    MiActionListener ml = new MiActionListener();
    
    btnCerrar.addActionListener(ml);
    
    this.add(btnCerrar);
    }
    
    public static void main(String args[]){
        Practica1 p = new Practica1();
        p.setVisible(true);
    }
}

class MiActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
    System.exit(0);
    }
}
