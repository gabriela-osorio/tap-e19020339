/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gaby-
 */
public class EjemploHilos {
    public static void main (String args []){
        Thread th1 = new Thread(new HiloRunnable());
        Thread th2 = new HiloThread();
        
        th1.start(); th2.start();
        
        try {
            th1.join();
            th2.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(EjemploHilos.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
        System.out.println("La ejecución del main finalizó");
    }
}
class HiloRunnable implements Runnable{

    @Override
    public void run() {
        for (int i=0; i<20; i++){
            System.out.println("Buenos Dias: " +i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         System.out.println("Terminó el hilo HiloRunnable");
        
    }
  
}
class HiloThread extends Thread{
    
     @Override
    public void run() {
        for (int i=100; i<120; i++){
            System.out.println("Buenas Noches: " +i);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
         System.out.println("Terminó el hilo HiloThread");
  
    }
    
}
